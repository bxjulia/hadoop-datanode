#!/bin/bash

# start ssh server
/etc/init.d/ssh start

# start datanode and nodemanager
$HADOOP_HOME/bin/hdfs --daemon start datanode
$HADOOP_HOME/bin/yarn --daemon start nodemanager

# check if all daemons are running
STATUS=$(jps)
if [[ $STATUS == *"NodeManager"* ]] && [[ $STATUS == *"DataNode"* ]]; then
  echo "DataNode and NodeManager running"
  
  # keep container running
  tail -f /dev/null
fi
